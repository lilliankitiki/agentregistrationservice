package za.co.inforwarestudios.agent_registration.repository;

/**
 * Created by Lillian on 2015/12/10.
 */

import com.google.common.base.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import za.co.inforwarestudios.agent_registration.entity.AgentEntity;

public interface AgentRepository extends JpaRepository<AgentEntity, Long> {
    Optional<AgentEntity> findByIdOrPassword(Long idOrPassword);

}

