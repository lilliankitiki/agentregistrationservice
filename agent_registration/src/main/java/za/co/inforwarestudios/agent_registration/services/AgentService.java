package za.co.inforwarestudios.agent_registration.services;

import org.springframework.beans.factory.annotation.Autowired;
import sun.management.Agent;

import sun.management.resources.agent;
import za.co.inforwarestudios.agent_registration.entity.AgentEntity;
import za.co.inforwarestudios.agent_registration.repository.AgentRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Lillian on 2015/12/10.
 */
public class AgentService {

    private final AgentRepository agentRepository;

    @Autowired

    public AgentService(AgentRepository agentRepository) {
        this.agentRepository = agentRepository;
    }

    public AgentEntity createAgent(AgentEntity agentEntity) throws IOException {

        AgentEntity agentEntity1;
        return agentEntity1 = agentRepository.save(agentEntity);
    }

    public List<AgentEntity> getAllAgents() {

        List<AgentEntity> agentList = new ArrayList<AgentEntity>();

        Iterator<AgentEntity> agentIterator = (Iterator<AgentEntity>) agentRepository.findAll();
        while (agentIterator.hasNext()) {
            agentList.add(agentIterator.next());
        }

        return agentList;
    }

    public List<AgentEntity> getAgent(long idOrPassword) throws IOException {
        List<AgentEntity> agentList = new ArrayList<AgentEntity>();
        Iterator<AgentEntity> agentIterator = (Iterator<AgentEntity>) agentRepository.findOne(idOrPassword);

        while (agentIterator.hasNext()) {
            agentList.add(agentIterator.next());
        }
        return agentList;
    }

    public AgentEntity updateAgent(AgentEntity agentEntity) throws IOException {
        AgentEntity agentEntity1;
        agentEntity1 = this.agentRepository.save(agentEntity);

        return agentEntity1;
    }
}



