package za.co.inforwarestudios.agent_registration.entity;

import javax.persistence.Entity;

/**
 * Created by Lillian on 2015/12/09.
 */
@Entity
public class AgentEntity {

    private String name;
    private String surname;
    private Long merchantID;
    private Long cellNumber;
    private Long idOrPassport;
    private String bankingDetails;
    private Long userCode;
    private Long pin;


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Long getMerchantID() {
        return merchantID;
    }

    public Long getCellNumber() {
        return cellNumber;
    }

    public Long getIdOrPassport() {
        return idOrPassport;
    }

    public String getBankingdetails() {
        return bankingDetails;
    }

    public Long getUserCode() {
        return userCode;
    }

    public Long getPin() {
        return pin;
    }
}
